extends Node

class_name Constants

#########################################################################
# MEMBERS
#########################################################################

const MEMBER_NAMES = [
	"Ελβίρα",
	"Παναγιωτης",
	"Çağatay",
	"Eman",
	"Ayşe",
]

const MEMBER_NATIONALITIES = [
	"CHYPRIOTE",
	"CHYPRIOTE",
	"CHYPRIOTE",
	"PAKISTANAISE",
	"CHYPRIOTE",
]

const MEMBER_FACES = [
	"res://assets/faces/Ελβίρα.png",
	"res://assets/faces/Παναγιωτης.png",
	"res://assets/faces/Çağatay.png",
	"res://assets/faces/Eman.png",
	"res://assets/faces/Ayşe.png",
]

const MEMBER_FLAGS = [
	"res://assets/faces/chypre_south.png",
	"res://assets/faces/chypre_south.png",
	"res://assets/faces/chypre_north.png",
	"res://assets/faces/pakistan.png",
	"res://assets/faces/chypre_north.png",
]


#########################################################################
# PLAYER
#########################################################################


const PLAYER_MOVE_SPEED = 160
const PLAYER_NODE_NAME = 'Player'

#########################################################################
# QUESTS
#########################################################################

const QUESTS_DISPLAY = [
	"MOSQUEE",
	"ID_CARD",
	"DRINK",
	"CHANGE",
	"SOUVLAKIS"
]

#########################################################################
# DIALOG VARIABLES
#########################################################################

const VARIABLE_HAS_ID_CARD = "has_id_card"
const VARIABLE_CHANGED_MONEY = "changed_money"

##########################################################################
# AUDIO
##########################################################################

const BG_MUSIC_BPM = 102.0
const BG_MUSIC_MEASURE_BEATS = 4
const BG_MUSIC_SONG_BEATS = 64

enum BG_MUSIC_TYPES {NORMAL, MILITARY, NORTH}

const BG_MUSIC_AUDIOS = {
	BG_MUSIC_TYPES.NORMAL : preload("res://assets/audio/main theme.ogg"),
	BG_MUSIC_TYPES.MILITARY : preload("res://assets/audio/military music 2.ogg"),
	BG_MUSIC_TYPES.NORTH : preload("res://assets/audio/Main theme nord.ogg")
}

##########################################################################
# SFX
##########################################################################
enum SFX {
	BELL, WIND_CALM, IMAM, COFEE,
	CITY_CALM, CITY_BUSY, BIRDS, FRIES, MONEY,
	MEMBER_ADD, MEMBER_REMOVE, OBJECT, WRITE
}

const SFX_AUDIOS = {
	SFX.IMAM : preload("res://assets/audio/sfx/appel priere court 16bitPCM  SFX.ogg"),
	SFX.COFEE : preload("res://assets/audio/sfx/Ambiance café.ogg"),
	SFX.BIRDS : preload("res://assets/audio/sfx/birds SFX.ogg"),
	SFX.MEMBER_ADD : preload("res://assets/audio/sfx/member_add.ogg"),
	SFX.MEMBER_REMOVE : preload("res://assets/audio/sfx/member_remove.ogg"),
	SFX.OBJECT : preload("res://assets/audio/sfx/object.ogg"),
	SFX.WRITE : preload("res://assets/audio/sfx/Write SFX.ogg"),
	SFX.MONEY : preload("res://assets/audio/sfx/Money SFX.ogg")
}

##########################################################################
# ENVIRONMENT
##########################################################################

const BIRD_FLY_SPEED = 220

