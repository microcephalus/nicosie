extends CanvasLayer

signal dialog_finished()

var _dialog_running := false
var dialog: Control = null
var _first := true

onready var _tween := $Tween
onready var _black := $Black

const map_scene = preload("res://scenes/map.tscn")
const menu_scene = preload("res://scenes/Menu.tscn")
const end_scene = preload("res://scenes/EndScreen.tscn")
const a_propos_scene = preload("res://scenes/APropos.tscn")

const FADE_DURATION = 0.5

func _ready() -> void:
	layer = 3
	_black.modulate = Color.transparent
	_black.hide()
	TranslationServer.set_locale('fr')

func go_to_game() -> void:
	_fade_to_scene(map_scene, 2)
	yield(get_tree().create_timer(FADE_DURATION, false), "timeout")
	start_dialog("01-famagusta_gate")

func go_to_menu() -> void:
	_fade_to_scene(menu_scene)

func go_to_end() -> void:
	_fade_to_scene(end_scene)

func go_to_a_propos() -> void:
	_fade_to_scene(a_propos_scene)

func start_dialog(timeline: String) -> void:
	if not _dialog_running:
		if not TranslationServer.get_locale().begins_with('fr'):
			timeline = TranslationServer.get_locale() + '-' + timeline

		dialog = Dialogic.start(timeline, _first)
		self.add_child(dialog)
		dialog.connect("event_end", self, "_on_dialogic_event_end")
		_dialog_running = true
		_first = false

func is_running() -> bool:
	return _dialog_running

func get_variable(var_name: String) -> String:
	return Dialogic.get_variable(var_name)

func _on_dialogic_event_end(type: String) -> void:
	if type == 'timeline':
		dialog = null
		_dialog_running = false
		self.emit_signal("dialog_finished")

func _fade_to_scene(scene: PackedScene, delay_fade=0) -> void:
	_black.show()
	_tween.interpolate_property(_black, "modulate", Color.transparent, Color.white, FADE_DURATION)
	_tween.start()
	yield(_tween, "tween_all_completed")

	get_tree().change_scene_to(scene)

	if delay_fade > 0:
		yield(get_tree().create_timer(delay_fade, false), "timeout")

	_tween.interpolate_property(_black, "modulate", Color.white, Color.transparent, FADE_DURATION)
	_tween.start()
	yield(_tween, "tween_all_completed")
	_black.hide()
