extends Node


signal close_checkpoint(pos)
signal open_checkpoint(pos)

signal member_added(member_id)
signal member_removed(member_id)

signal quest_changed(quest_id)

signal id_card_acquired
signal livres_acquired

func add_member(args: Array) -> void:
	emit_signal("member_added", int(args[0]))

func remove_member(args: Array) -> void:
	emit_signal("member_removed", int(args[0]))

func change_quest(args: Array) -> void:
	emit_signal("quest_changed", int(args[0]))

func acquire_id_card() -> void:
	emit_signal("id_card_acquired")

func acquire_livres() -> void:
	emit_signal("livres_acquired")
