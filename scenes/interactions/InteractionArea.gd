extends Area2D

class_name InteractionArea

signal interaction_finished(node)

export var dialog_timeline: String

onready var _sprite := $Sprite

var _active := false
onready var _original_scale: Vector2 = _sprite.scale

func _ready() -> void:
	deactivate()

func _on_InteractionArea_body_entered(body: Node) -> void:
	if _active and body.name == Constants.PLAYER_NODE_NAME:
		deactivate()
		DialogManager.start_dialog(dialog_timeline)
		yield(DialogManager, "dialog_finished")
		_on_interaction_finished()

func _process(_delta: float) -> void:
	var scale := 0.8 + 0.4 * abs(sin(4 * OS.get_ticks_msec() / 1000.0))
	_sprite.scale = scale * _original_scale

func activate() -> void:
	_active = true
	visible = true

func deactivate() -> void:
	_active = false
	visible = false

func _on_interaction_finished() -> void:
	emit_signal("interaction_finished", self)
