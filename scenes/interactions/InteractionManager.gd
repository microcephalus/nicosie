extends Node2D

onready var _02_mur = $"02_Mur"
onready var _03_maison = $"03_Maison"
onready var _04_checkpoint_sud = $"04_CheckpointSud"
onready var _05_checkpoint_nord = $"05_CheckpointNord"
onready var _06_mosquee = $"06_Mosquee_Selimiye"
onready var _07_hoi_polloi = $"07_HoiPolloi"
onready var _08_distributeur = $"08_Distributeur"
onready var _09_retour_checkpoint_nord = $"09_RetourCheckpointNord"
onready var _10_retour_checkpoint_sud = $"10_RetourCheckpointSud"
onready var _11_souvlakis = $"11_Souvlakis"
onready var _12_vieux_nord = $"12_VieuxNord"

onready var _all_interactions = get_children()

signal _passed_04()
signal _passed_07()

func _ready() -> void:
	for interaction_area in _all_interactions:
		interaction_area.connect("interaction_finished", self, "_on_interaction_finished")

	_run_scenario()

func _run_scenario() -> void:
	for i in [_02_mur, _03_maison, _04_checkpoint_sud]:
		i.activate()

	yield(self, "_passed_04")
	_deactivate_all()
	Events.emit_signal("open_checkpoint", "south")

	_05_checkpoint_nord.activate()
	yield(_05_checkpoint_nord, "interaction_finished")
	Events.emit_signal("open_checkpoint", "north")
	Events.emit_signal("close_checkpoint", "south")

	_06_mosquee.activate()
	yield(_06_mosquee, "interaction_finished")
	Events.emit_signal("close_checkpoint", "north")

	for i in [_07_hoi_polloi, _08_distributeur, _12_vieux_nord]:
		i.activate()

	yield(self, "_passed_07")
	_deactivate_all()

	_09_retour_checkpoint_nord.activate()
	yield(_09_retour_checkpoint_nord, "interaction_finished")
	_12_vieux_nord.deactivate()
	Events.emit_signal("open_checkpoint", "north")

	_10_retour_checkpoint_sud.activate()
	yield(_10_retour_checkpoint_sud, "interaction_finished")
	Events.emit_signal("open_checkpoint", "south")
	Events.emit_signal("close_checkpoint", "north")

	_11_souvlakis.activate()
	yield(_11_souvlakis, "interaction_finished")

	DialogManager.go_to_end()

func _deactivate_all() -> void:
	for interaction_area in _all_interactions:
		interaction_area.deactivate()

func _on_interaction_finished(node: InteractionArea) -> void:
	if node == _02_mur or node == _12_vieux_nord:
		node.activate()

	elif node == _08_distributeur:
		_07_hoi_polloi.activate()

	elif node == _03_maison:
		_04_checkpoint_sud.activate()
	elif node == _04_checkpoint_sud:
		if DialogManager.get_variable(Constants.VARIABLE_HAS_ID_CARD) == "true":
			emit_signal("_passed_04")
		else:
			_03_maison.activate()

	elif node == _07_hoi_polloi and DialogManager.get_variable(Constants.VARIABLE_CHANGED_MONEY) == "true":
		emit_signal("_passed_07")
