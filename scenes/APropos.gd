extends Control

onready var _already_clicked := false

func _gui_input(event: InputEvent) -> void:
	if event.is_action_released("next"):
		_clicked()

func _input(event: InputEvent) -> void:
	if event.is_action_released("next"):
		_clicked()

func _clicked() -> void:
	if not _already_clicked:
		DialogManager.go_to_menu()
		_already_clicked = true

func _ready() -> void:
	var locale := TranslationServer.get_locale()
	$TextureRect.texture.current_frame =  0 if locale == 'fr' else 1
