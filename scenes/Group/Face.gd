extends Control

onready var name_field : RichTextLabel= $Name
onready var nationality : RichTextLabel= $Nationality
onready var face_sprite : TextureRect = $Face
onready var animation : AnimationPlayer = $AnimationPlayer
onready var flag_sprite : TextureRect = $Flag

func update_with_member(member_id: int):
	name_field.text = Constants.MEMBER_NAMES[member_id]
	nationality.text = tr(Constants.MEMBER_NATIONALITIES[member_id])
	face_sprite.texture = load(Constants.MEMBER_FACES[member_id])
	flag_sprite.texture = load(Constants.MEMBER_FLAGS[member_id])
	animation.play('Squash')
	AudioManager.play_sfx(Constants.SFX.MEMBER_ADD)

func remove():
	animation.play("Remove")
	AudioManager.play_sfx(Constants.SFX.MEMBER_REMOVE)

func _on_AnimationPlayer_animation_finished(anim_name):
		if anim_name == "Remove":
			queue_free()
