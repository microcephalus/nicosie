extends StaticBody2D

onready var _sprite := $AnimatedSprite
onready var _collision := $CollisionShape2D

export(String, "south", "north") var checkpoint_position = "south"

func _ready() -> void:
	if checkpoint_position == "south":
		_sprite.flip_h = true
	Events.connect("close_checkpoint", self, "close_checkpoint")
	Events.connect("open_checkpoint", self, "open_checkpoint")
	_sprite.play("close")

func open_checkpoint(pos: String) -> void:
	if pos == checkpoint_position:
		_sprite.play("open")
		_collision.disabled = true

func close_checkpoint(pos: String) -> void:
	if pos == checkpoint_position:
		_sprite.play("close")
		_collision.disabled = false
