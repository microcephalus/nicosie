extends Control

onready var id_card := $IdCard
onready var livres := $Livres
onready var id_card_animation := $IdCard/AnimationPlayer
onready var livres_animation := $Livres/AnimationPlayer

func _ready() -> void:
	id_card.hide()
	livres.hide()

	Events.connect("id_card_acquired", self, "add_id_card")
	Events.connect("livres_acquired", self, "add_livres")

func add_id_card():
	id_card.show()
	id_card_animation.play("Squash")
	AudioManager.play_sfx(Constants.SFX.OBJECT)

func add_livres():
	livres.show()
	livres_animation.play("Squash")
	AudioManager.play_sfx(Constants.SFX.OBJECT)
