extends TextureRect

onready var animation = $AnimationPlayer
onready var label = $CenterContainer/RichTextLabel

var current_quest: int = 0

func _ready() -> void:
	Events.connect("quest_changed", self, "display_new_quest")

func display_new_quest(quest_id: int):
	current_quest = quest_id
	label.text = tr(Constants.QUESTS_DISPLAY[quest_id])
	animation.play("AppearBubble")

func play_write_sound():
	AudioManager.play_sfx(Constants.SFX.WRITE)
