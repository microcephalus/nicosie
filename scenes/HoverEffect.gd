extends TextureRect

func _ready() -> void:
	visible = false

func on_hover() -> void:
	visible = true

func off_hover() -> void:
	visible = false
