extends Node2D


var fleeing = false
var direction = Vector2.RIGHT
var birds_fly_speed = Constants.BIRD_FLY_SPEED

# Called when the node enters the scene tree for the first time.
func _ready():
	$Area2D/AnimatedSprite.frame = randi()%6
	$Area2D/AnimatedSprite.flip_h = bool(randi()%2)
	$Area2D/AnimatedSprite.speed_scale = rand_range(0.8, 1.2)
	pass # Replace with function body.

func _process(delta):
	if fleeing:
		position.x += direction.x*delta*birds_fly_speed
		position.y += direction.y*delta*birds_fly_speed
		

func _on_Area2D_body_entered(body: Node) -> void:
	if body.name == Constants.PLAYER_NODE_NAME:
		$Area2D/AnimatedSprite.animation = "flying"
		fleeing = true
		$Area2D/fly.play()
		#pick a direction
		direction = direction.rotated(rand_range(0.0, PI*2))
		$Area2D/DespawnTimer.start()


func _on_DespawnTimer_timeout():
	self.queue_free()
