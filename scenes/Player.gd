extends KinematicBody2D

signal position_changed(new_pos)

var velocity: Vector2

onready var sprite = $Sprite
onready var foot_steps = $Footsteps

var _touch_movements : bool

func _ready() -> void:
	_touch_movements = OS.has_touchscreen_ui_hint()
	self.name = Constants.PLAYER_NODE_NAME



func _physics_process(delta) -> void:
	if DialogManager.is_running():
		update_sprite(Vector2.ZERO, Vector2.DOWN)
		return

	if _touch_movements:
		if Input.is_mouse_button_pressed(BUTTON_LEFT):
			var destination = get_global_mouse_position()
			var direction = destination - global_position
			var norm_direction = direction.normalized()
			velocity = Constants.PLAYER_MOVE_SPEED * norm_direction
			if velocity != Vector2.ZERO:
				self.move_and_slide(velocity)
				emit_signal("position_changed", self.position)
			update_sprite(velocity, norm_direction)
	else:
		var direction := Vector2.ZERO
		if Input.is_action_pressed("ui_left"):
			direction.x -= 1
		if Input.is_action_pressed("ui_right"):
			direction.x += 1
		if Input.is_action_pressed("ui_up"):
			direction.y -= 1
		if Input.is_action_pressed("ui_down"):
			direction.y += 1

		var norm_direction = direction.normalized()
		velocity = Constants.PLAYER_MOVE_SPEED * norm_direction
		if velocity != Vector2.ZERO:
			self.move_and_slide(velocity)
			emit_signal("position_changed", self.position)

		update_sprite(velocity, norm_direction)

func update_sprite(velocity: Vector2, looking_direction: Vector2):
	if velocity.length() <= 0:
		if foot_steps.playing:
			foot_steps.stop()
		sprite.animation = "idle"
		sprite.play()
	else:
		var foot_steps = $Footsteps
		if not foot_steps.playing:
			foot_steps.play()
		if looking_direction.y < 0:
			sprite.animation = "walking_up"
		else:
			sprite.animation = "walking_down"


