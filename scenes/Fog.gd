extends Sprite

var fogImage = Image.new()
var fogTexture = ImageTexture.new()

func _ready():
	fogImage.create(1920, 1080, false, Image.FORMAT_RGBA8)
	fogImage.fill(Color.white)
	update_fog_image_texture()

func update_fog_image_texture():
	fogTexture.create_from_image(fogImage)
	self.texture = fogTexture
