extends TextureRect

var _clicked := false

onready var audio = $AudioStreamPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	AudioManager.stop_bg_music()
#	$Jouer.grab_focus()

func _start_game() -> void:
	audio.stop()
	AudioManager.start_bg_music()
	DialogManager.go_to_game()

func _on_Jouer_pressed():
	if not _clicked:
		_clicked = true
		_start_game()

func _on_A_propos_pressed() -> void:
	if not _clicked:
		_clicked = true
		DialogManager.go_to_a_propos()

func _on_Language_pressed() -> void:
	if TranslationServer.get_locale() == 'fr':
		_change_locale('en')
	else:
		_change_locale('fr')

func _change_locale(locale: String) -> void:
	TranslationServer.set_locale(locale)

	var index := 0 if locale == 'fr' else 1
	$Language.texture_normal.current_frame = index
	$Language.texture_hover.current_frame = index
	$Jouer.texture_normal.current_frame = index
	$Jouer.texture_hover.current_frame = index
	$A_propos.texture_normal.current_frame = index
	$A_propos.texture_hover.current_frame = index
	$Title.texture.current_frame = index
