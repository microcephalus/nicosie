extends VBoxContainer

const _face_node = preload("res://scenes/Group/Face.tscn")

func _ready() -> void:
	Events.connect("member_added", self, "add_member")
	Events.connect("member_removed", self, "remove_member")

func add_member(member_id: int):
	var node = _face_node.instance()
	node.name = str(member_id)
	add_child(node)
	node.update_with_member(member_id)

func remove_member(member_id: int):
	var node = get_node(str(member_id))
	node.remove()

