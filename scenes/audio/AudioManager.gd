extends Node

const SFXAudioStream = preload("res://scenes/audio/SFXAudioStream.tscn")

signal beat(position)
signal measure_beat(position)
signal bpm_changed(bpm)
#signal rank(rank)

var _base_db = 0

var _current_bg_track = Constants.BG_MUSIC_TYPES.NORMAL
var _next_bg_track = Constants.BG_MUSIC_TYPES.NORMAL
var _background = false
var _band_bus_db = 0
var _band_bus_index = 1
var _sfx_bus_index = 2
const _REVERB_FILTER_INDEX = 0
const _LOW_PASS_FILTER_INDEX = 1

func _ready():
	_band_bus_index = AudioServer.get_bus_index("BackgroundMusic")
	_sfx_bus_index = AudioServer.get_bus_index("SFX")
#	_band_bus_db = AudioServer.get_bus_volume_db(_band_bus_index)
#	start_band()


#func _set_current_bg_track(track):
#	_current_bg_track = track if track in Constants.TRACKS else Constants.TRACKS.PHASE_1

#func _get_audio_from_track_instrument(track, instrument):
#	if track == Constants.TRACKS.PHASE_2:
#		return Constants.AUDIOS_2[instrument]
#	elif track == Constants.TRACKS.PHASE_3:
#		return Constants.AUDIOS_3[instrument]
#	elif track == Constants.TRACKS.PHASE_4:
#		return Constants.AUDIOS_4[instrument]
#	return Constants.AUDIOS_1[instrument]
#

# starts the BGMusicConductor
func start_bg_music():
	$BGMusicConductor.play()

func stop_bg_music():
	$BGMusicConductor.stop()

func play_sfx(sfx: int):
	var streamPlayer = SFXAudioStream.instance()
	streamPlayer.set_stream(Constants.SFX_AUDIOS[sfx])
	streamPlayer.type = sfx
#	only works for OGG files !
	streamPlayer.stream.loop = false
	add_child(streamPlayer)


func change_current_bg_track(track:int):
	_current_bg_track = track
	#audiotrack update
	$BGMusicConductor._switch_BGMusic(track)

func set_background(is_bg:bool):
	_background = is_bg
	var db_offset = 6.0
	if _background:
		AudioServer.set_bus_effect_enabled(_band_bus_index,0,true)
		AudioServer.set_bus_effect_enabled(_band_bus_index,1,true)
		AudioServer.set_bus_volume_db(_band_bus_index, _band_bus_db - db_offset)
	else:
		AudioServer.set_bus_effect_enabled(_band_bus_index,0,false)
		AudioServer.set_bus_effect_enabled(_band_bus_index,1,false)
		AudioServer.set_bus_volume_db(_band_bus_index, _band_bus_db + db_offset)

func is_background():
	return _background

func _emit_beat_signals(last_reported_beat, measure_beat):
	if not _background:
		emit_signal("beat", last_reported_beat)
		emit_signal("measure_beat", measure_beat)

func _on_AudioManager_beat(position):
	$Debug/BeatLabel.text = "BEAT : "+str(position)
	pass


func _on_AudioManager_measure_beat(position):
	$Debug/MeasureBeatLabel.text = "MESURE BEAT : "+str(position)
	if _next_bg_track != _current_bg_track and position == 1:
		change_current_bg_track(_next_bg_track)

func change_bg_track_on_next_measure(next_track):
	_next_bg_track = next_track

# test purpose
func _on_Timer_timeout():
	print("T1")
	start_bg_music()
#	set_background(true)
	pass


func _on_Timer2_timeout():
#	set_background(false)
	pass

func _on_Timer3_timeout():
	print("T3")
	change_bg_track_on_next_measure(Constants.BG_MUSIC_TYPES.MILITARY)
	pass

func _on_Timer4_timeout():
	print("T4")
	change_bg_track_on_next_measure(Constants.BG_MUSIC_TYPES.NORMAL)
	pass
