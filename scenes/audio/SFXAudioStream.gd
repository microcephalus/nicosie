extends AudioStreamPlayer

var type setget set_type
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_type(new_type):
	type = new_type

func _on_SFXAudioStream_finished():
	queue_free()
