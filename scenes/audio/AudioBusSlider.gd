extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(String,"Master", "BackgroundMusic", "SFX", "Footsteps", "Environment") var audio_bus_name = "Master"

onready var sound_icon := $SoundIcon
onready var slider := $VSlider

var _audio_bus_index := 0
# Called when the node enters the scene tree for the first time.
func _ready():
	_audio_bus_index = AudioServer.get_bus_index(audio_bus_name)
	if _audio_bus_index == -1:
		_audio_bus_index = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_VSlider_value_changed(value):
	sound_icon.texture_normal.current_frame = 1 if value == 0 else 0

	AudioServer.set_bus_volume_db(_audio_bus_index,linear2db(value))


func _on_SoundIcon_pressed() -> void:
	slider.value = 0.5 if slider.value == 0 else 0
