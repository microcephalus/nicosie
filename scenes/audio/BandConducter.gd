extends Node

onready var BGMusicStreamPlayer = $BGMusicNormal
var BG_MUSIC_TYPE_PLAYING = Constants.BG_MUSIC_TYPES.NORMAL

# audio caracteristics
var bpm = Constants.BG_MUSIC_BPM
var measure_beats = Constants.BG_MUSIC_MEASURE_BEATS
var song_beats = Constants.BG_MUSIC_SONG_BEATS

# Tracking the beat and song position
var song_position = 0.0
var song_position_in_beats = 0
var sec_per_beat = 60.0 / bpm
var last_reported_beat = 0
# var beats_before_start = 0
var measure_beat = 1

# Determining how close to the beat an event is
var closest = 0
var time_off_beat = 0.0
# if song played more than half the song for loop detection
var passed_half_loop = false

func _initialize():
	sec_per_beat = 60.0 / bpm
	song_position = 0.0
	song_position_in_beats = 0
	measure_beat = 1
	last_reported_beat = 0

func _ready():
	_initialize()

func play():
	BGMusicStreamPlayer.play()
	
func stop():
	BGMusicStreamPlayer.stop()

func reset():
	BGMusicStreamPlayer.seek(0)
	_initialize()
	
func is_playing():
	return BGMusicStreamPlayer.is_playing()

func change_bpm(new_bpm, new_song_beats):
	bpm = new_bpm
	song_beats = new_song_beats
	_initialize()


func _switch_BGMusic(bg_music_type):
	if bg_music_type == BG_MUSIC_TYPE_PLAYING :
		return
	BGMusicStreamPlayer.stop()
	if bg_music_type == Constants.BG_MUSIC_TYPES.MILITARY :
		BGMusicStreamPlayer = $BGMusicMilitary
		BG_MUSIC_TYPE_PLAYING = bg_music_type
	elif bg_music_type == Constants.BG_MUSIC_TYPES.NORTH :
		BGMusicStreamPlayer = $BGMusicNorth
		BG_MUSIC_TYPE_PLAYING = bg_music_type
	else:
		BGMusicStreamPlayer = $BGMusicNormal
		BG_MUSIC_TYPE_PLAYING = Constants.BG_MUSIC_TYPES.NORMAL
	BGMusicStreamPlayer.play()
	BGMusicStreamPlayer.seek(song_position)


func _update_song_position():
	# get real song_position given hardware latency and chunk mixing
	song_position = BGMusicStreamPlayer.get_playback_position() + AudioServer.get_time_since_last_mix()
	song_position -= AudioServer.get_output_latency()
	song_position = max(song_position, 0.0)
	song_position_in_beats = int(floor(song_position / sec_per_beat))# + beats_before_start
#	print(song_position_in_beats, measure_beat)

func _physics_process(_delta):
	if is_playing():
		_update_song_position()
		_report_beat()


func _report_beat():
	if passed_half_loop and last_reported_beat < song_beats/2:
		passed_half_loop = false
	# if we entered a new beat
	if last_reported_beat < song_position_in_beats or (last_reported_beat >= song_beats-1 and song_position_in_beats == 0):
		# if the song has looped
		last_reported_beat = song_position_in_beats % song_beats
		# update mesure_beat
		measure_beat = last_reported_beat % measure_beats
		# send signal for beat
		get_parent()._emit_beat_signals(last_reported_beat+1, measure_beat+1)
		if not passed_half_loop and last_reported_beat > song_beats/2:
			passed_half_loop = true


# play the target audio stream starting at the given beat
func play_from_beat(target_audio_stream : AudioStreamPlayer, beat):# , offset):
	target_audio_stream.play(beat * sec_per_beat)
	measure_beat = beat % measure_beats
	return target_audio_stream

# sync the target audio stream with the playing metronome
func play_synched_with_metronome(target_audio_stream : AudioStreamPlayer):# , offset):
	_update_song_position()
	if is_playing():
		target_audio_stream.play()
		target_audio_stream.seek(song_position)
	return target_audio_stream


func _on_BandConducter_tree_exiting():
	pass
