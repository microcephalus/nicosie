extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_MilitaryMusicZone_body_entered(body: Node) -> void:
	if body.name == Constants.PLAYER_NODE_NAME:
		AudioManager.change_bg_track_on_next_measure(Constants.BG_MUSIC_TYPES.MILITARY)


func _on_MilitaryMusicZone_body_exited(body: Node2D) -> void:
	if body.name == Constants.PLAYER_NODE_NAME:
		# if body left zone from north
		if body.global_position.y < $Collision2D.global_position.y:
			AudioManager.change_bg_track_on_next_measure(Constants.BG_MUSIC_TYPES.NORTH)
		else:
			# if body left zone from south
			AudioManager.change_bg_track_on_next_measure(Constants.BG_MUSIC_TYPES.NORMAL)
