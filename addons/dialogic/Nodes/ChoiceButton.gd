extends Button

var _already_pressed := false
var _require_manual_focus := true

signal answer_chosen

func _input(event):
	if Input.is_action_just_released("next") and _is_mouse_on_button() and _require_manual_focus:
		_pressed()

func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseMotion and _is_mouse_on_button() or Input.is_action_just_pressed("next") and _is_mouse_on_button():
		_require_manual_focus = true
		grab_focus()

func _process(_delta: float) -> void:
	if has_focus():
		modulate = Color.bisque
	else:
		modulate = Color.white

func _pressed() -> void:
	if not _already_pressed:
		emit_signal("answer_chosen")
		_already_pressed = true

#func _on_ChoiceButton_pressed() -> void:
#	_pressed()

func _is_mouse_on_button() -> bool:
	return get_global_rect().has_point(get_global_mouse_position())
